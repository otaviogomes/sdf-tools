#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  8 10:48:32 2019

@author: otaviogomes
"""

import matplotlib
import os
import struct
import time
from datetime import date
from datetime import datetime
import struct
import sys
import signal
import argparse
import matplotlib.pyplot as plt

SDF_DATA_SECTION_OFFSET = 2048
SDF_FIRST_STREAM_HEADER_OFFSET = 256
SDF_SENSOR_STREAM_HEADER_SIZE = 256
SDF_FILE_HEADER_SIZE = 256

def signal_handler(signal, frame):
    print(";)")
    sys.exit(0)

def sdf_parse_header_info(fd):
    
    # create an empty dictionary to store header data
    header = dict();
    
    # get file signature
    signature = fd.read(4);
    header['signature'] = str(signature, 'utf-8')
    
    # move file pointer to the header version
    fd.seek(4,0)
    header_version = fd.read(4)
    header['version'] = int.from_bytes(header_version, byteorder='little', signed=False)

    time_stamp = fd.read(4)    
    header['timestamp'] = int.from_bytes(time_stamp, byteorder='little', signed=False)

    # get the number of the signals in the file
    num_signals = fd.read(4)
    header['signals'] = int.from_bytes(num_signals, byteorder='little', signed=False)

    # create an empty list of dictionaries with signal metadata
    signals = list();
        
    for k in range(header['signals']):
        
        # compute signal header offset
        offset = SDF_SENSOR_STREAM_HEADER_SIZE * k + SDF_FILE_HEADER_SIZE;
        
        # move file pointer to the first stream header
        fd.seek(offset,0)
        
        # fetch signal index
        index = fd.read(4)
        index = int.from_bytes(index, byteorder='little', signed=False)

        # fetch bytes per sample
        bytes_per_sample = fd.read(4)
        bytes_per_sample = int.from_bytes(bytes_per_sample, byteorder='little', signed=False)
        
        # fetch sample rate
        sample_rate = fd.read(4)
        sample_rate = int.from_bytes(sample_rate, byteorder='little', signed=False)
        
        signals.append({'index':index,'bytes_per_sample':bytes_per_sample, 'sample_rate':sample_rate})        
        
    return header,signals

def sdf_get_block_list(fd, header, signals):
    
    datablocks = list()
    [datablocks.append([]) for k in range (header['signals'])]

    # move file pointer to the beginning of the data section
    fd.seek(SDF_DATA_SECTION_OFFSET,0)
    
    while True:
        
        try:
    
            # get signal index
            signal_index = fd.read(4)
            signal_index = int.from_bytes(signal_index, byteorder='little', signed=False)
        
            if signal_index:
    
                # skip time stamp
                fd.seek(4,1)
                
                # fetch the number of samples
                sample_count = fd.read(4)
                sample_count = int.from_bytes(sample_count, byteorder='little', signed=False)
                
                # get the position of the datablock in file
                position = fd.tell()
                
                # get the datablock size in bytes
                chunk_size = sample_count * signals[signal_index-1]['bytes_per_sample']
                
                # append to the list at a given signal index a tuple containing the position and the chunk size in bytes
                datablocks[(signal_index-1)].append(tuple([position, chunk_size]))
                
                # skip datablock
                fd.seek(chunk_size,1)
                
            else:
                raise
                
        except:
                break
            
    return datablocks

def sdf_get_signal(fd, a, b, c, signal_index):
    
    # create an empty list
    signal = list()
    
    # iterate over a tuple
    for position,chunk_size in c[signal_index-1]:

        fd.seek(position,0)
        data = fd.read(chunk_size)
        sample_count = int(chunk_size / b[signal_index-1]['bytes_per_sample'])
        fmt = '<' + 'i'*sample_count
        integer_from_bytes = struct.unpack(fmt, data)
        
        signal.extend(list(integer_from_bytes))
        
    return signal

def sdf_write_to_text(filename, signal):
    
    with open(filename, 'w') as fd:
        for k in signal:
            fd.write("{}\n".format(k))

def sdf_print_header(header):
    sys.stdout.write("signature: {}\n".format(header['signature']))
    sys.stdout.write("number of signal found: {}\n".format(header['signals']))
    sys.stdout.write("timestamp: " + datetime.fromtimestamp(header['timestamp']).strftime('%Y-%m-%d %H:%M:%S') + "\n")
       
def main(argv):
    
    # install a signal handler to break infine loop
    signal.signal(signal.SIGINT, signal_handler)
    
    # create an argparse object
    parser = argparse.ArgumentParser(description="Convert a SDF file to a standard text file which can be imported in any application")
    
    # add parser arguments
    parser.add_argument('-v','--verbose', help='Verbose output', action='store_true')
    parser.add_argument('inputfile', help='Input SDF file')
    parser.add_argument('-o', '--output', help='Output text file name', nargs='?')
    parser.add_argument('-s','--show-graph', help='Plot signals', action='store_true')
    parser.add_argument('-c','--convert', help='Convert SDF to text file', action='store_true')
    
    # parsing args
    args = parser.parse_args()
    
    # input filename
    base_filename = args.inputfile
    input_filename = base_filename
    
    # output filename
    if args.output:
        output_filename = args.output
    else:
        output_filename = base_filename
        
    # open sdf file in read mode
    with open(input_filename, "rb") as fd:
        
        # extract sdf metadata
        header,signals_info = sdf_parse_header_info(fd)
        
        if args.verbose:
            sdf_print_header(header)
        
        # create a plot figure    
        if args.show_graph:
            fig, axs = plt.subplots(header['signals'])     
        
        # get blocklist
        block_list = sdf_get_block_list(fd,header,signals_info)
        
        for k in range(header['signals']):
            signal_index = k + 1
            signal_data = sdf_get_signal(fd,header,signals_info,block_list, signal_index)
            
            # output to text file
            if args.convert:
                filename = output_filename + '_' + '{:02d}'.format(signal_index)
                sdf_write_to_text(filename, signal_data)       

            # add a plot
            if args.show_graph:
                axs[k].plot(signal_data)
            
    # show signal plot
    if args.show_graph:
        plt.show()
   
        # prevent the proccess to return - the signal handler will break this
        # loop
        while True:
            pass

    # exit with code 0     
    sys.exit(0)
        
if __name__ == "__main__":
    main(sys.argv[1:])